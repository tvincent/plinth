#!/usr/bin/python3
# -*- mode: python -*-
#
# This file is part of FreedomBox.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Configuration helper for FreedomBox general configuration.
"""

import argparse
import os

import augeas

from plinth import action_utils
from plinth.modules.config import (APACHE_HOMEPAGE_CONF_FILE_NAME,
                                   FREEDOMBOX_APACHE_CONFIG)


def parse_arguments():
    """Return parsed command line arguments as dictionary."""
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='subcommand', help='Sub command')

    set_home_page = subparsers.add_parser(
        'set-home-page',
        help='Set the home page for this FreedomBox instance.')
    set_home_page.add_argument('homepage',
                               help='path to the webserver home page')

    subparsers.add_parser('reset-home-page',
                          help='Reset the homepage of the Apache server.')

    subparsers.required = True
    return parser.parse_args()


def subcommand_set_home_page(arguments):
    """Set the default app for this FreedomBox."""
    conf_file_path = os.path.join('/etc/apache2/conf-available',
                                  APACHE_HOMEPAGE_CONF_FILE_NAME)

    redirect_rule = 'RedirectMatch "^/$" "{}"\n'.format(arguments.homepage)

    with open(conf_file_path, 'w') as conf_file:
        conf_file.write(redirect_rule)

    action_utils.webserver_enable('freedombox-apache-homepage')


def subcommand_reset_home_page(_):
    """Sets the Apache web server's home page to the default - /plinth."""
    config_file = FREEDOMBOX_APACHE_CONFIG
    default_path = 'plinth'

    aug = augeas.Augeas(
        flags=augeas.Augeas.NO_LOAD + augeas.Augeas.NO_MODL_AUTOLOAD)
    aug.set('/augeas/load/Httpd/lens', 'Httpd.lns')
    aug.set('/augeas/load/Httpd/incl[last() + 1]', config_file)
    aug.load()

    aug.defvar('conf', '/files' + config_file)

    for match in aug.match('/files' + config_file +
                           '/directive["RedirectMatch"]'):
        if aug.get(match + "/arg[1]") == '''"^/$"''':
            aug.set(match + "/arg[2]", '"/{}"'.format(default_path))

    aug.save()


def main():
    """Parse arguments and perform all duties."""
    arguments = parse_arguments()

    subcommand = arguments.subcommand.replace('-', '_')
    subcommand_method = globals()['subcommand_' + subcommand]
    subcommand_method(arguments)


if __name__ == '__main__':
    main()
